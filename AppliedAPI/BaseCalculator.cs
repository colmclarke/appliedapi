﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AppliedAPI.Helpers;

namespace AppliedAPI
{
	public abstract class BaseCalculator
	{
		public BaseCalculator()
		{
		}

		public decimal InitialPremium { get; set; } = 500;
		public int MinimunDriverAge { get; set; } = 17;
		public int MaximumDriverAge { get; set; } = 75;

		public int HighRiskAge { get; set; } = 25;


		public virtual decimal ApplyOccupationAlterations(string occupation, decimal premium)
		{
			occupation = occupation.Trim().ToLower();
			//Occupation Calculations
			var currentAmount = premium;

			if (occupation == "accountant")
			{
				currentAmount = SubtractPercentage(10, currentAmount);
			}
			if (occupation == "chauffeur")
			{
				currentAmount = AddPercentage(10, currentAmount);
			}
			return currentAmount;
		}


		public virtual decimal ApplyAgeAlterations(int age, decimal premium)
		{
			//Age Calculations
			var currentAmount = premium;

			if (age >= MinimunDriverAge && age <= HighRiskAge)
			{

				currentAmount = AddPercentage(20, premium);
			}
			else if (age >=26 && age <=75)
			{
				//validation has already been done but we dont want to assume this wont be reused
				currentAmount = SubtractPercentage(10, premium);
			}
			return currentAmount;
		}



		public virtual decimal CalculatePremuim(DateTime startDate, DateTime dob, string occupation)
		{
			decimal calculatedRate = InitialPremium;
			int driverAge = AgeCalculations.CalculateAgePolicyStartDate(dob, startDate); //age at start of policy
			ValidatePolicy(driverAge, startDate);
			calculatedRate = ApplyOccupationAlterations(occupation, calculatedRate);
			calculatedRate = ApplyAgeAlterations(driverAge, calculatedRate);
			

			return calculatedRate;
		}




		public virtual bool ValidatePolicy(int driverAge, DateTime startDate)
		{
			if (startDate < DateTime.Now)
			{
				throw new ArgumentOutOfRangeException("Policy start date cannot be in the past");
			}
			if (driverAge < MinimunDriverAge)
			{
				throw new ArgumentException("Driver is too young");
			}
			if (driverAge > MaximumDriverAge)
			{
				throw new ArgumentException("Driver too old");
			}

			//Applicant is approved
			return true;
		}

		decimal AddPercentage(decimal percent, decimal value)
		{
			var f = value / 100 * percent;
			return value + f;
		}
		 decimal SubtractPercentage(decimal percent, decimal value)
		{
			var f = value / 100 * percent;
			return value - f;
		}
	}
}
