﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppliedAPI
{
	public interface ICalculator
	{
		 decimal InitialPremium { get; set; } 
		 int MinimunDriverAge { get; set; } 
		 int MaximumDriverAge { get; set; }
		 int HighRiskAge { get; set; }

		decimal ApplyOccupationAlterations(string occupation, decimal premium);

		decimal ApplyAgeAlterations(int age, decimal premium);

		decimal CalculatePremuim(DateTime startDate, DateTime dob, string occupation);
		
		bool ValidateDriver(int driverAge, DateTime startDate);
		
	}
}
