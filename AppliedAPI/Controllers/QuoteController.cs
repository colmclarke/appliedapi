﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace AppliedAPI.Controllers
{
	

    [Route("/[controller]")]
    [ApiController]
    public class QuoteController : ControllerBase
    {
		private PremiumCalculator _premiumCalculator;

		public IActionResult Get(string startDate, string name, string occupation, string birthdate)
		{

			_premiumCalculator = new PremiumCalculator();

			DateTime sDate = Convert.ToDateTime(startDate);
			DateTime dob = Convert.ToDateTime(birthdate);
			decimal value;
			try
			{
				value = _premiumCalculator.CalculatePremuim(sDate, dob, occupation);
			}
			catch (Exception ex)
			{

				return BadRequest(ex.Message);
	
			}
			return Ok(value);
		}

	}
}