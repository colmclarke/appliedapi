﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppliedAPI.Helpers
{
	public static class AgeCalculations
	{
		public static int CalculateAgePolicyStartDate(DateTime dob, DateTime startDate)
		{
			if (startDate.Year < dob.Year)
			{
				throw new InvalidOperationException("are you from the future bro?");
			}
			int age = 0;
			age = startDate.Year - dob.Year;
			if (startDate.DayOfYear < dob.DayOfYear)
				age = age - 1;

			return age;

		}
	}
}
