﻿using AppliedAPI;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ApiTests
{
	public class PremiumCalculatorTests
	{

		private readonly PremiumCalculator _premiumCalculator;

		public PremiumCalculatorTests()
		{
			_premiumCalculator = new PremiumCalculator();
		}




		[Fact]
		public void InValidPolicyInPast()
		{
			var startDate = DateTime.Now.AddDays(-1);
			var driverDob = DateTime.Now.AddYears(-90);
			var occupation = "accountant";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Policy start date should not be valid


			Assert.Throws<ArgumentOutOfRangeException>(() => _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation));
			
		}


		[Fact]
		public void InValidPolicyTooOld()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = DateTime.Now.AddYears(-90);
			var occupation = "accountant";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation deduction 
			// Should applu 10% age deduction

			var expectedMessage = "Driver too old";

			var exception = Assert.Throws<ArgumentException>(() => _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation));
			Assert.Equal(expectedMessage, exception.Message);
		}

		[Fact]
		public void InValidPolicyTooYoung()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = DateTime.Now.AddYears(-15); //Done this way to avoid tests breaking in a few years
			var occupation = "accountant";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation deduction 
			// Should applu 10% age deduction

			var expectedMessage = "Driver is too young";

			var exception = Assert.Throws<ArgumentException>(() => _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation));
			Assert.Equal(expectedMessage, exception.Message);
		}


		[Fact]
		public void ValidPolicyAccontant24()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = DateTime.Now.AddYears(-24).AddDays(10);
			var occupation = "accountant";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation deduction 
			// Should apply 20% age increase

			var result = _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation);

			Assert.Equal(540, result);
		}

		[Fact]
		public void ValidPolicyChauffeur24()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = DateTime.Now.AddYears(-24).AddDays(10);
			var occupation = "chauffeur";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation increase 
			// Should apply 20% age increase

			var result = _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation);

			Assert.Equal(660, result);
		}


		[Fact]
		public void ValidPolicyAccontant30()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = new DateTime(1990, 01, 01);
			var occupation = "accountant";


			var initValue =_premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation deduction 
			// Should applu 10% age deduction

			var result = _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation);

			Assert.Equal(405, result);
		}

		[Fact]
		public void ValidPolicyChauffeur30()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = new DateTime(1990, 01, 01);
			var occupation = "chauffeur";


			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should apply 10% occupation increase 
			// Should applu 10% age deduction

			var result = _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation);

			Assert.Equal(495, result);
		}


		[Fact]
		public void UnknownOccupation()
		{
			var startDate = DateTime.Now.AddDays(10);
			var driverDob = new DateTime(1990, 01, 01);
			var occupation = "engineer";
		
			var initValue = _premiumCalculator.InitialPremium; //500

			// Driver should be valid
			// Should ingore occupation alterations
			// Should applu 10% age deduction

			var result = _premiumCalculator.CalculatePremuim(startDate, driverDob, occupation);

			Assert.Equal(450, result);
		}


	}
}
