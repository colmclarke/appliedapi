﻿using AppliedAPI;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ApiTests
{
	public class DriverValidationTests
	{

		private readonly PremiumCalculator _premiumCalculator;

		public DriverValidationTests()
		{
			_premiumCalculator = new PremiumCalculator();
		}

		[Fact]
		public void CheckInvalidDriverAge()	{
			//checks the age of driver at policy start date
			var driverAge = 16;
			var policyFutureDate = DateTime.Now.AddDays(10);

			Assert.Throws<ArgumentException>(() => _premiumCalculator.ValidatePolicy(driverAge, policyFutureDate));
		}


		[Fact]
		public void CheckDriverTooOldMessage()
		{
			var message = "Driver too old";
		
			var driverAge = 80;
			var policyFutureDate = DateTime.Now.AddDays(10);

			var exception = Assert.Throws<ArgumentException>(() => _premiumCalculator.ValidatePolicy(driverAge, policyFutureDate));
			Assert.Equal(message, exception.Message);
		}


		[Fact]
		public void CheckDriverTooYoungMessage()
		{
			var message = "Driver is too young";

			var driverAge = 16;
			var policyFutureDate = DateTime.Now.AddDays(10);

			var exception = Assert.Throws<ArgumentException>(() => _premiumCalculator.ValidatePolicy(driverAge, policyFutureDate));
			Assert.Equal(message, exception.Message);
		}


		[Fact]
		public void CheckValidDriverAgeFutureDate()
		{
			//checks the age of driver at policy start date
			var driverAge = 17;
			var policyFutureDate = DateTime.Now.AddDays(10);

			var result = _premiumCalculator.ValidatePolicy(driverAge, policyFutureDate);

			Assert.True(result);
		}


		[Fact]
		public void CheckValidDriverAgePastDate()
		{
			//checks the age of driver at policy start date
			var driverAge = 17;
			var policyPastDate = DateTime.Now.AddMonths(-1);

			Assert.Throws<ArgumentOutOfRangeException>(() => _premiumCalculator.ValidatePolicy(driverAge, policyPastDate));
		}

	}
}
