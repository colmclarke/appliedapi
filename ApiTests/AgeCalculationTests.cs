using AppliedAPI.Helpers;
using System;
using Xunit;

namespace ApiTests
{
	public class AgeCalculationTests
	{

		[Fact]
		public void TestValidAge()
		{
			var dob = new DateTime(1950, 01, 01);
			var currentDate = new DateTime(2010, 01, 01);
			var result = AgeCalculations.CalculateAgePolicyStartDate(dob, currentDate);

			Assert.Equal(60, result);
		}

		[Fact]
		public void TestFutureDOB()
		{
			var dob = new DateTime(2015, 01, 01);
			var currentDate = new DateTime(2010, 01, 01);

			Assert.Throws<InvalidOperationException>(() => AgeCalculations.CalculateAgePolicyStartDate(dob, currentDate));
		}
	}
}
