﻿using AppliedAPI;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ApiTests
{
	public class OccupationTests
	{
		private readonly PremiumCalculator _premiumCalculator;

		public OccupationTests()
		{
			_premiumCalculator = new PremiumCalculator();
		}

		[Fact]
		public void OccupationAccountantValid()
		{
			//should apply 10% discount 
			var premium = 100;
			var result = _premiumCalculator.ApplyOccupationAlterations("accountant", premium);

			Assert.Equal(90, result);
		}
		[Fact]
		public void OccupationChauffeurVald()
		{
			//should apply 10% increase 
			var premium = 100;
			var result = _premiumCalculator.ApplyOccupationAlterations("chauffeur", premium);

			Assert.Equal(110, result);
		}

		[Fact]
		public void InvalidOccupation()
		{
			//exect nothing to happen
			var premium = 100;
			var result = _premiumCalculator.ApplyOccupationAlterations("cooper", premium);

			Assert.Equal(100, result);
		}

		[Fact]
		public void UpperCaseOccupation()
		{
			//exect nothing to happen
			var premium = 100;
			var result = _premiumCalculator.ApplyOccupationAlterations("ACCOUNTANT", premium);

			Assert.Equal(90, result);
		}
	}
}
