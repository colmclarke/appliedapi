﻿using AppliedAPI;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ApiTests
{
	public class AgeAlteractionTests
	{

		private readonly PremiumCalculator _premiumCalculator;

		public AgeAlteractionTests()
		{
			_premiumCalculator = new PremiumCalculator();
		}


		[Fact]
		public void ApplyAgePenalty()
		{
			// age between 17 & 25 apply 20% increase
			var age = 23;
			var premium = 100;
			var result = _premiumCalculator.ApplyAgeAlterations(age, premium);

			Assert.Equal(120, result);
		}

		[Fact]
		public void ApplyAgeDiscount()
		{
			// age between 17 & 25 apply 10% reduction
			var age = 26;
			var premium = 100;
			var result = _premiumCalculator.ApplyAgeAlterations(age, premium);

			Assert.Equal(90, result);
		}


		[Fact]
		public void UnderMinimumAge()
		{
			//should be over and including 17, nothing should happen
			var age = 16;
			var premium = 100;
			var result = _premiumCalculator.ApplyAgeAlterations(age, premium);

			Assert.Equal(100, result);
		}

		[Fact]
		public void OverMaximumAge()
		{
			//should be under 75, nothing should happen
			var age = 90;
			var premium = 100;
			var result = _premiumCalculator.ApplyAgeAlterations(age, premium);

			Assert.Equal(100, result);
		}
	}
}
